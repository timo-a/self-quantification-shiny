#' return a subset of the dataframe with respect to user input
#'
#' @param df The full dataframe
#' @param rangeInDays An integer specifying the window size we look at
#' @param offset A positive integer indicating how far back the window is shifted.
#'               Per default, (offset=0) the window ends at the last row of df
#'               and starts `rangeInDays` rows(i.e. days) earlier.
#' @param weekday Integer specifying a weekday that should be focussed on (within the range)
#'                1=Monday, 7=Sunday, outside [1,9]= all days
#'
#'
applyUserQuery <- function(df,
                           rangeInDays,
                           offset = 0,
                           weekday = 0) {
  takeAwayOffset <- df[1:max(0, nrow(df) - offset), ] #max to prevent neg
  ranged  <- tail(takeAwayOffset, rangeInDays)
  final <- filterOnWeekday(ranged, weekday)
  return (final)
}


#' filter df for weekday between 1(Mon) and 7(Sun) in `date` column
#' if `weekday` outside 1..7, nothing happens
filterOnWeekday <- function(df, weekday = 0) {
  if (weekday %in% seq(1, 7)) {
    df <- df[wday(df$date, week_start = 1) == weekday, ]
  }
  return(df)
}


#' Returns yyyy-ww where last week of year `a` and first week of year `a+1` are conected if neccessary
#' he first few days of the new year may be assigned to the last year so that every uweek has 7 days
#'
#' @return  yyyy-ww as character, isoyear and week respectively, every unique week has 7 days
#'
uniqueweek <- function(date) {
  return(sprintf("%04d-%02d", isoyear(date), isoweek(date)))
}

#' returns yyyy-mm
uniquemonth <- function(date) {
  return(sprintf("%d-%02d", year(date), month(date)))
}

weekdayNames <-
  c("Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday")

#' turns a date and a bedtime column into one column of POSIXct where times past midnight are counted as next day
#'
#' @param datecol A column of dates, as.Date
#' @param timecol A column of times of the day as characters
#'
#' @examples
#'
make_bed_time <- function(datecol, timecol) {
  bedtime_in_hours_since_0000 <-
    as.difftime(timecol , "%H:%M", unit = "hours")
  days_where_bedtime_was_next_day <-
    as.integer(bedtime_in_hours_since_0000 < 15)
  offset <-
    as.difftime(days_where_bedtime_was_next_day,
                format = "%d",
                unit = "days")
  correct_date <- datecol + offset
  
  datetime_asstring <- paste(as.character(correct_date), timecol)
  datetime_asPOSIXct <-
    strptime(datetime_asstring, format = "%Y-%m-%d %H:%M", tz = "Europe/Berlin")
  return(datetime_asPOSIXct)
  
}

#library(gplots)
#library('Hmisc')
require(plyr) #for desc
library(dplyr) #for %>%
library(lubridate)
library(ggplot2)


#' Display factor for each day in year-blocks of 7x52
#'
#' @param data A Dataframe with columns "Date" and "Factors".
#' @param colors A list of colors equal in number and order as their respective factors.
#' @param weekday_ticks vector with custom labels for days of the week starting at Monday. Default is c("M",...,"S")
#'
#' @return plot
#'
#' @examples
#' factor_board_by_year(data.frame(Date=c("2019-09-01","2019-09-02"),
#'                                 Factors=factor(c("yellow","red"),
#'                                                levels=c("green", "yellow", "red"),
#'                                                ordered=TRUE)),
#'                                 colors=c("#00FF00","#FFFF00","#FF0000"))
#'
factor_board_by_year <-
  function(data,
           colors,
           weekday_ticks = c("M", "T", "W", "T", "F", "S", "S")) {
    data <- transform(
      data,
      year = as.POSIXlt(Date)$year + 1900,
      week = strftime(Date, format = "%W"),
      #lubridate::week would work as well but we try to avoid imports
      wday = wday(Date, week_start = 1)
    )
    
    
    ggplot(data, aes(
      x = week,
      y = reorder(wday, plyr::desc(wday)),
      fill = Factors
    )) +
      scale_y_discrete(labels = weekday_ticks,
                       limits = c(7, 6, 5, 4, 3, 2, 1)) +
      labs(y = "") + #no ylabel, self explanatory
      geom_tile(colour = "white") +
      scale_fill_manual(values = colors) +
      facet_wrap( ~ year, ncol = 2) +
      coord_fixed() +
      theme(axis.text.x = element_text(angle = 80))
  }


#' Display one bar chart per month and the distribution of factors per month as layers within a bar.
#'
#' @param data A Dataframe with columns "Date" and "Factors".
#' @param colors A list of colors equal in number and order as their respective factors.
#' @param weekday_ticks vector with custom labels for days of the week starting at Monday. Default is c("M",...,"S")
#'
#' @return plot
#'
#' @examples
#' distribution_bar_per_month(data.frame(Date=c("2019-08-31","2019-09-01","2019-09-02"),
#'                                       Factors=factor(c("yellow","red","green"),
#'                                                      levels=c("green", "yellow", "red"),
#'                                                      ordered=TRUE)),
#'                                       colors=c("#00FF00","#FFFF00","#FF0000"))
#'
#'
distribution_bar_per_month <-
  function(data,
           colors,
           weekday_ticks = c("M", "T", "M", "T", "F", "S", "S")) {
    #TODO: try #factors line plots? But maybe they overlap and are hard to distinguish...
    #      or an exploding distribution bar where every factor's segment is vertically centered
    #                                             and padded s.t. no two factors overlap?
    #
    
    data1 <- transform(data,
                       umonth = uniquemonth(Date))
    
    #add percentage of diet per month
    data2 = data1 %>%
      group_by(umonth, Factors) %>%
      summarise(n = n()) %>%
      mutate(freq = n / sum(n))
    
    data2a = as.data.frame(data2)
    
    #group by umonth
    #https://stackoverflow.com/a/20011526/3014199
    data3 <- ddply(data2a,
                   ~ umonth,
                   plyr::summarize,
                   Factors = Factors,
                   perc = freq)
    
    ggplot(data3, aes(x = umonth, y = perc, fill = Factors)) + geom_bar(stat =
                                                                          "identity") +
      scale_y_continuous(name = "", labels = scales::percent) +
      scale_x_discrete(name = "" , labels = format_tick) +
      theme(axis.text.x = element_text(angle = 80, hjust = 1)) +
      scale_fill_manual(values = colors)
    
  }
format_tick <- function(x) {
  return(ifelse(substring(x, 6, 8) == "01", x, paste0("", substring(x, 6, 8))))
}




#' Bar plots with one bar per week
#'
#' @param data A dataframe of `week` as string and
#'                            `amount` as number
#'
#' @return plot
#'
#' @examples
#'
#'
binary_factor_weeksum_by_year <- function(data, colors) {
  ggplot(data, aes(x = week, y = amount)) +
    #labs(y = "") + #no ylabel, self explanatory
    geom_col(colour = "white") +
    scale_fill_manual(values = colors) +
    facet_wrap( ~ year, ncol = 2) #+
  #coord_fixed()
}



factor_month_by_day_plot <-
  function(df, colors, myweekdaynames = weekdayNames) {
    #weekdayNames aus timeUtils.r
    
    df2 <- df %>%  mutate(
      date = ymd(date),
      weekday = lubridate::wday(date, week_start = 1),
      #monthday = mday(date),
      monthday = strftime(date, "%d"),
      week   = isoweek(date),
      umonth = uniquemonth(date)
    )
    
    ggplot(df2, aes(monthday, factor(umonth, levels = rev(levels(
      factor(umonth)
    ))))) +
      geom_raster(aes(fill = factors)) +
      coord_fixed() +
      scale_x_discrete(position = "top",
                       breaks = c("05", "10", "15", "20", "25", "30")) +
      labs(y = "week") +
      scale_fill_manual(values = colors) +
      scale_y_discrete(breaks = c(
        "2015-10",
        "2016-01",
        "2017-01",
        "2018-01",
        "2019-01",
        "2020-01"
      )) #TODO read breaks from data
    
  }


#' difference plots
#'
#' @param col A logical column
#'
#'
#'
#' @examples diff_plot(c(TRUE, FALSE, TRUE))
#'
diff_plot <- function(col) {
  differences <- diff(which(col), lag = 1)
  x <- 1:length(differences)
  plot(x, differences, 'p')
}

habit_stats_vec <- function(vector) {
  col = c()
  #total
  col = append(col, mean(vector))
  
  #since first managed to to
  since_begin = vector[which(vector == T)[1]:length(vector)]
  col = append(col, mean(since_begin))
  
  #last 4 weeks
  col = append(col, mean(tail(vector, 28)))
  #last week
  col = append(col, mean(tail(vector, 7)))
  return(col)
}

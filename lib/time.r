#'
#' @param data A dataframe containing a column time of class POSIXct
#'
#' @examples
#' tcol <- strptime(x=c("22:00","21:00", "21:05"), format="%H:%M")
#' ocol <- c("a","b","c") #a second column is needed for some reason
#' time_scatter(20,0,0, data.frame(xy=ocol,   #name doesn't matter
#'                                 time=tcol))
#'
time_scatter <- function(rangeInDays,
                         offset = 0,
                         weekday = 0,
                         data) {
  df <- applyUserQuery(data, rangeInDays, offset, weekday)
  time <- df$time
  stripchart(
    as.numeric(time),
    pch = 20,
    main = paste("last", round(rangeInDays / 7, 2), "weeks"),
    method = "jitter",
    jitter = 1,
    xaxt = "n"
  )
  axis(side = 1, pretty(time), format(pretty(time), "%H:%M"))
}

#' averages for caption..
#'
#' @param timevec A vector of POSIXct
#'
#' @examples
#' timestats( strptime(x=c("22:00","21:00", "21:05"), format="%H:%M") )
#'
timestats <- function(timevec) {
  stats = format(c(mean(timevec), median(timevec)), "%H:%M")
  names(stats) = c("mean", "median")
  return(stats)
}

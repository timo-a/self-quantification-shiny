#functions and plots concerning get-up and go-to-bed and times

require(plyr)

library(dplyr)
library(lubridate)
library(ggplot2)

library(zoo)

source("lib/time.r")
source("lib/timeUtils.r")


data <-
  read.csv(
    tablefile,
    header = T,
    stringsAsFactors = FALSE,
    sep = ","
  ) #TODO try date as index


data$date <-
  as.Date(data$date, tz = "UTC") #timezone doesn't seem to get recognized... put utc just in case


data$up <-
  strptime(data$getuptime, format = "%H:%M", tz = "Europe/Berlin") #converts to CET/CEST automatically

#' time and date of going to bed as POSIXct
data$bed_full  <- make_bed_time(data$date, data$gobedtime)

#' date as POSIXct summertime as in Berlin
data$date_CEsT <-
  strptime(as.character(data$date), format = "%Y-%m-%d", tz = "Europe/Berlin")
#ensures date parsed as CET/CEST

#' timediff that can exceed 24h
data$bed_since_midnight <-
  difftime(data$bed_full, data$date_CEsT, unit = 'mins')

#data$bed as time when user went to bed but projected to today
#e.g. if today is 2016-02-15
#and on the dates 2016-02-10 and 2016-02-11 the user went to bed at 0:10 (i.e. on the next day, the 11th) and 23:30 respectively
#then the values in data$bed are "2016-02-16 0:10" and "2016-02-15 23:30"
data$bed <-
  strptime("0:00", format = "%H:%M") + data$bed_since_midnight


#for correlation plots, maybe unify with $bed, $up sometime
data$up_time <-
  as.difftime(data$getuptime, "%H:%M", unit = "mins") #
data$bed_time <- data$bed_since_midnight #


#' dataframe with get up info, used in all get_up plots
Data_up <- data.frame(date = data$date, time = data$up)


#' dataframe with go to bed info, used in all go to bed plots
Data_bed <- data.frame(date = data$date, 
                       time = data$bed,
                       time_since_midnight = data$bed_since_midnight)
Data_bed <- Data_bed[which(!is.na(Data_bed$time)), ]#drop na:s


#' for correlation plots
Data_bed_up  <-
  data.frame(date = data$date,
             bed_time = data$bed,
             up_time = data$up)
Data_bed_up2 <-
  data.frame(
    date = data$date[-1],
    bed_time = head(data$bed_time, -1),
    up_time = data$up_time[-1]
  )



#' line plot, when did I get up
get_up_plot <- function(rangeInDays, offset, weekday) {
  df <- applyUserQuery(Data_up, rangeInDays, offset, weekday)
  
  stats <- timestats(na.omit(df$time))
  
  with(df, plot(
    date,
    time,
    type = "b",
    main = "Get up times",
    sub = paste("mean:", stats["mean"], "median:", stats["median"])
  ))
  ticksAt <- c(1:6)
  axis(2, at = ticksAt, labels = sort(df$time)[ticksAt])
}
get_up_scatter <- function(rangeInDays, offset, weekday) {
  time_scatter(rangeInDays, offset, weekday, Data_up)
}

#TODO: do it in ggplot, isolate title, extract commonalities
get_up_plot_variance <- function(rangeInDays, offset, weekday) {
  df <- applyUserQuery(Data_up, rangeInDays, offset, weekday)
  as_minutes <- lubridate::hour(df$time) * 60 + lubridate::minute(df$time)
  print(as_minutes)
  total_var <- var(na.omit(as_minutes))
  variance <-
    rollapply(
      as_minutes,
      width = 15,
      FUN = var,
      align = "center",
      fill = NA,
      na.rm = T,
      partial = T
    )
  
  with(df,
       plot(
         date,
         variance,
         type = "b",
         main = "Get up times - Variance",
         sub = paste(
           "Variance (15-day-window) in the observed time range (time in minutes):",
           round(total_var)
         )
       ))
  ticksAt <- c(1:6)
  axis(2, at = ticksAt, labels = sort(variance)[ticksAt])
}

## go to bed tab

to_bed_plot <- function(rangeInDays, offset, weekday) {
  df <- applyUserQuery(Data_bed, rangeInDays, offset, weekday)
  
  stats = timestats(df$time)
  
  with(df,
       plot(
         date,
         time,
         type = "b",
         main = "Go to bed times",
         sub = paste("mean:", stats["mean"], "median:", stats["median"])
       ))
  ticksAt <- c(1:6)
  axis(2, at = ticksAt, labels = sort(df$time)[ticksAt])
}
to_bed_scatter <- function(rangeInDays, offset, weekday) {
  time_scatter(rangeInDays, offset, weekday, Data_bed)
}

to_bed_plot_variance <- function(rangeInDays, offset, weekday) {
  df <- applyUserQuery(Data_bed, rangeInDays, offset, weekday)
  as_minutes <- df$time_since_midnight #  lubridate::hour(df$time) * 60 + lubridate::minute(df$time)
  total_var <- var(na.omit(as_minutes))
  variance <-
    rollapply(
      as_minutes,
      width = 15,
      FUN = var,
      align = "center",
      fill = NA,
      na.rm = T,
      partial = T
    )
  #variance <- na.omit(variance)
  with(df,
       plot(
         date,
         variance,
         type = "b",
         main = "Go to bed times - Variance",
         sub = paste(
           "Variance (15-day-window) in the observed time range (time in minutes):",
           round(total_var)
         )
       ))
  ticksAt <- c(1:6)
  axis(2, at = ticksAt, labels = sort(variance)[ticksAt])
}



## sleep tab


#sleeplength
sleeptime_plot <- function(rangeInDays, offset, weekday) {
  #filter data according to parameters
  i = which(!is.na(data$bed)) #I started recording getuptimes before gobedtimes
  df = data.frame(date = data$date[i],
                  up = data$up[i + 1],
                  bed = data$bed[i]) #offset 'up' by 1 so getting up on yyyy-02-04
  #is next to going to bed on yyyy-02-03
  sleeplength <- applyUserQuery(df, rangeInDays, offset, weekday)
  
  notime = strptime("24:00", "%H:%M") #als uhrzeit geht einfacher
  sleeplength$time = sleeplength$up + (notime - sleeplength$bed) #time slept following that day
  
  stats = timestats(na.omit(sleeplength$time))
  with(sleeplength,
       plot(
         date,
         time,
         type = "h",
         main = "Sleep",
         sub = paste("mean:", stats["mean"], "median:", stats["median"])
       ))
  sleeplength
}

sleep_both <- function(rangeInDays, offset, weekday) {
  dff = data.frame(date = data$date,
                   up = data$up,
                   bed = data$bed)
  sleeplength = applyUserQuery(dff, rangeInDays, offset, weekday)
  df = sleeplength
  a <- min(sleeplength$up,  na.rm = T)
  b <- max(sleeplength$bed, na.rm = T)
  
  plot(
    df$date,
    df$up,
    type = "b",
    ylim = c(a, b),
    ann = T,
    main = "Get up and go to bed",
    xlab = "date",
    ylab = "time"
  )
  lines(df$date, df$bed, type = "b")
}



# if I go to bed later do I get up later?

#weekday is the day I get up
bed_up_correlation_plot <- function(rangeInDays, offset, weekday) {
  data_bu <- applyUserQuery(Data_bed_up, rangeInDays, offset, weekday)
  ggplot(data_bu, aes(x = bed_time, y = up_time)) +
    geom_point(shape = 1) +
    theme(
      panel.grid.major = element_blank()
      ,
      panel.grid.minor = element_blank()
      ,
      panel.background = element_blank()
    ) +
    labs(caption = sprintf("correlation: %5f", cor(
      as.numeric(data_bu$bed_time),
      as.numeric(data_bu$up_time)
    ))) +
    ggtitle("Does getting up earlier result in going to bed earlier?") +
    xlab("go to bed time") + ylab("get up time")
  
}

# if I go to bed later do I get up later?
bed_up_diff_correlation_plot <-
  function(rangeInDays, offset, weekday) {
    #I really don't see a better way
    #If I choose time as a format, -2h will be displayed as 22:00 -> have to go with time spans, e.g. difftime
    #diff returns seconds
    #use hms to convert to hms
    #use format_hm to drop seconds
    
    df_query <-
      applyUserQuery(Data_bed_up2, rangeInDays, offset, weekday)
    to_bed_diff = diff(df_query$bed_time)
    up_diff  =    diff(df_query$up_time)
    
    ggplot(data = NULL, aes(
      x = hms::as.hms(to_bed_diff),
      y = hms::as.hms(up_diff)
    )) +
      geom_point(shape = 1) +
      
      theme(
        panel.grid.major = element_blank()
        ,
        panel.grid.minor = element_blank()
        ,
        panel.background = element_blank()
      ) +
      scale_x_time(name = "go to bed time", labels = format_hm) +
      scale_y_time(name = "get up time",    labels = format_hm) +
      ggtitle("Does earlier bed time result in getting up earlier?") +
      labs(caption = sprintf("correlation: %5f", cor(
        as.numeric(to_bed_diff),
        as.numeric(up_diff)
      ))) +
      
      geom_hline(yintercept = 0) +
      geom_vline(xintercept = 0)
    
  }
#' convert time to HH:MM
format_hm <- function(sec)
  stringr::str_sub(format(sec), end = -4L)

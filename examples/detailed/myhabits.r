# functions and plots concerning habits

library(gplots)
library('Hmisc')
require(plyr)

library(dplyr)
library(lubridate)
library(ggplot2)
library(zoo)

source("lib/timeUtils.r")
source("lib/habits.r")

globalVariables(c("meat", "teeth_breakfast"))

#to avoid columns overwritten by other files
data_h <-
  read.csv(
    tablefile,
    header = T,
    sep = ",",
    strip.white = TRUE,
    stringsAsFactors = FALSE
  )

#only until now
#data = data[1:which(data_h$date == Sys.Date()),]
data_h$date <- as.Date(data_h$date)


data_h$vacuuming[is.na(data_h$vacuuming)] <- F


### habits ###
options(digits = 2)#decimal precision

habit_stats <- function() {
  z <- matrix(nrow = 4, ncol = 4)
  z[1, ] <- habit_stats_vec(data_h$french_read)
  z[2, ] <- habit_stats_vec(data_h$french_listen)
  z[3, ] <- habit_stats_vec(data_h$french_speak)
  z[4, ] <- habit_stats_vec(data_h$jogging)
  
  frame <- data.frame(z)
  colnames(frame) <- c("total", "since first", "4weeks", "week")
  rownames(frame) <- c("french_read", "french_listen", "french_write", "jogging")
  frame
}



excercise_plot <- function() {
  firstDone <- which(data_h$jogging)[1]
  dropped <- data_h[firstDone:nrow(data), ]
  df <- data.frame(
    date = dropped$date,
    factors = factor(
      dropped$jogging,
      levels = c(FALSE, TRUE),
      labels = c("skipped", "done"),
      ordered = TRUE
    )
  )
  
  
  factor_month_by_day_plot(df, colors = c("#101044", "#DDDD00"))
  
}


vegetarian_plot <- function() {
    #TODO this plot (with coord_fixed) is not as wide as the others (without coord_fixed)
    
    firstDone <- which(data_h$meat != "")[1]
    dropped   <- data_h[firstDone:nrow(data), ]
    
    levels_cow_to_vegan <- c('cow', 'pork', 'chicken', 'fish', 'vegetarian', 'vegan')
    
    veg.data <- data.frame(Date    = dropped$date,
                           Factors = factor(data_h$meat,
                                            levels = rev(levels_cow_to_vegan),
                                            ordered = T))
    
    colors_red_to_green <- c("#ff0000", "#ff7700",  "#ffaa00",
                             "#7777ff", "#77ff00", "#00ff00")
    
    factor_board_by_year(veg.data, colors = rev(colors_red_to_green))
    
  }

vegetarian_plot_distribution_bars <- function() {
  firstDone <- which(data_h$meat != "")[1]
  dropped   <- data_h[firstDone:nrow(data), ]
  
  levels_cow_to_vegan <- c('cow', 'pork', 'chicken', 'fish', 'vegetarian', 'vegan')

  veg.data <- data.frame(Date    = dropped$date,
                         Factors = factor(data_h$meat,
                                          levels = rev(labels_cow_to_vegan),
                                          ordered = T))
  
  colors_red_to_green <- c("#ff0000", "#ff7700", "#ffaa00",
                           "#7777ff", "#77ff00", "#00ff00"  )
  
  distribution_bar_per_month(veg.data, colors = rev(colors_red_to_green))
  
}

vegetarian_plot_4week_aggregate <- function() {
  firstDone <- which(data_h$meat != "")[1]
  dropped   <- data_h[firstDone:nrow(data), ]
  
  veg <- data.frame(
    date = dropped$date,
    meat = ifelse(dropped$meat %in% c("vegetarian", "vegan"), 0, 1),
    week = strftime(dropped$date, format = "%W"),
    uweek = uniqueweek(dropped$date),
    year = strftime(dropped$date, format = "%Y")
  )
  
  meatsum_by_week <-
    aggregate(veg$meat,
              by = list(Category = veg$uweek),
              FUN = sum)$x
  meat_of_last4weeks_by_week <-
    zoo::rollsumr(meatsum_by_week, 4,  fill = 0)
  
  df <- data.frame(
    "amount" = meat_of_last4weeks_by_week,
    "uweek"  = unique(veg$uweek),
    "week"   = substring(as.character(unique(veg$uweek)), 6, 7),
    "year"   = substring(as.character(unique(veg$uweek)), 1, 4)
  )
  
  saveRDS(df, file = "debug.csv")
  
  binary_factor_weeksum_by_year(df)
}

vegetarian_plot_meat_per_week <- function() {
  firstDone <- which(data_h$meat != "")[1]
  dropped   <- data_h[firstDone:nrow(data), ]
  
  veg <- data.frame(
    date = dropped$date,
    meat  = ifelse(dropped$meat %in% c("vegetarian", "vegan"), 0, 1),
    week  = strftime(dropped$date, format = "%W"),
    uweek = uniqueweek(dropped$date),
    year  = strftime(dropped$date, format = "%Y")
  )
  meatsum_by_week <-
    aggregate(veg$meat,
              by = list(Category = veg$uweek),
              FUN = sum)$x
  
  df <- data.frame(
    "amount" = meatsum_by_week,
    "uweek"  = unique(veg$uweek),
    "week"   = substring(as.character(unique(veg$uweek)), 6, 7),
    "year"   = substring(as.character(unique(veg$uweek)), 1, 4)
  )
  
  binary_factor_weeksum_by_year(df)
}

vegetarian_plot_running_mean_per_week <- function() {
  #TODO: add two preview lines indicating best/worst possible average for future weeks
  
  firstDone <- which(data_h$meat != "")[1]
  dropped   <- data_h[firstDone:nrow(data), ]
  
  veg <- data.frame(
    date  = dropped$date,
    meat  = ifelse(dropped$meat %in% c("vegetarian", "vegan"), 0, 1),
    week  = strftime(dropped$date, format = "%W"),
    uweek = uniqueweek(dropped$date),
    year  = strftime(dropped$date, format = "%Y")
  )
  
  #count meat days every week
  meatsum_by_week <-
    aggregate(veg$meat,
              by = list(Category = veg$uweek),
              FUN = sum)$x
  
  
  
  df <-
    data.frame(
      "amount" = meatsum_by_week,
      #cummean(meatsum_by_week),
      "uweek"  = unique(veg$uweek),
      "week"   = substring(as.character(unique(veg$uweek)), 6, 7),
      "year"   = substring(as.character(unique(veg$uweek)), 1, 4)
    )
  
  
  df2 <- df %>% dplyr::arrange(year)  %>%
    dplyr::group_by(year) %>%
    dplyr::mutate(amount.running.mean = cummean(amount)) %>%
    dplyr::select(year, amount.running.mean, week)
  
  data = df2
  ggplot(data, aes(x = week, y = amount.running.mean)) +
    #labs(y = "") + #no ylabel, self explanatory
    geom_col(colour = "white") +
    scale_fill_manual(values = colors) +
    facet_wrap( ~ year, ncol = 2) +
    coord_fixed()
  
  
}


vegetarian_stats <- function() {
  firstDone <- which(data_h$v != "")[1]
  dropped   <- data_h[firstDone:nrow(data), ]
  v_f <- dropped$v[which(dropped$v != "")]
  c <- plyr::count(v_f)
  v <- c[which(c$x == 'v'), 'freq']
  f <- c[which(c$x == 'f'), 'freq']
  overall_percentage_meat = f / (v + f)
  return(overall_percentage_meat)
}

teeth_stats <- function() {
  z <- matrix(nrow = 4, ncol = 4)
  z[1, ] <-
    habit_stats_vec(data_h$teeth_breakfast) #TODO those are binary values! make them T,F
  z[2, ] <- habit_stats_vec(data_h$teeth_lunch)
  z[3, ] <- habit_stats_vec(data_h$teeth_dinner)
  z[4, ] <- habit_stats_vec(data_h$teeth_floss)
  frame <- data.frame(z)
  colnames(frame) <- c("total", "since first", "4weeks", "week")
  rownames(frame) <- c("breakfast", "lunch", "dinner", "floss")
  
  frame <-
    rbind(frame, frame["breakfast", ] + frame["lunch", ] + frame["dinner", ])
  row.names(frame)[5] <- "avg per day"
  frame
}


teeth_freq <- function(rangeInDays, offset) {
  #for now just do since beginning of the year, but eventually a timeseries would be nice
  #colReduced = data.frame(data_h$z_v, data_h$z_m, data_h$z_a)
  #untilOffset = colReduced[1:max(0,nrow(colReduced)-offset),]
  #sleeplength = tail(untilOffset ,rangeInDays)
  #tmp=sleeplength
  
  firstDone <- which(data_h$date == "2019-01-01")[1]
  
  dropped   <- data_h[firstDone:nrow(data), ]
  tmp = data.frame(dropped$teeth_breakfast,
                   dropped$teeth_lunch,
                   dropped$teeth_dinner)
  times_a_day = apply(tmp, 1, sum) #per col sum up all True values
  counts = as.data.frame(prop.table(table(times_a_day))) #rel freq frame. colnames: times_a_day, Freq
  colnames(counts) <-
    c("times_a_day", "freq") #fix them in case variable names change again
  
  ggplot(counts, aes(
    x = times_a_day,
    y = cumsum(freq),
    ymin = 0,
    ymax = cumsum(freq)
  )) +
    geom_pointrange(color = "red") +
    scale_y_continuous(
      breaks = seq(0, 1, .1),
      labels = scales::percent,
      #convert to %
      limits = c(0, 1)
    ) +
    labs(
      x = "",
      y = "",
      title = "distribution of #times brushed in a day since start of the year",
      subtitle = "red is cumulative"
    ) +
    theme_minimal() +
    
    geom_pointrange(data = counts, aes(
      x = times_a_day,
      y = freq,
      ymin = 0,
      ymax = freq
    ))
  
}


t_diff_plot <- function() {
  diff_plot(data_h$vacuum)
}

import sys

cached_year_month = "1990-01"

with open(sys.argv[1]) as f:
    for l in f.readlines():
        ym = l[:7]
        tail = l[8:]
        if ym != cached_year_month:
            cached_year_month = ym
            print(ym)

        if tail[0] == '0':
            tail = ' ' + tail[1:]
        print(tail,end='')

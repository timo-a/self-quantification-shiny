import sys

def tList2str(*x):
    return '[{}]'.format(','.join(x))

def list2str(*x):
    return '["{}"]'.format('","'.join(x))

def list2str2(*x):
    return '[["{}"]]'.format('"],["'.join(x))

print(tList2str('Date "date"',
                'TimeUp "getuptime"',
                'BinaryFac "jogging"',
                'MultiFac {} "rls"'.format(list2str("french_read", "french_listen", "french_speak")),
                'HexMultiFac ' + list2str("teeth_floss", "teeth_dinner", "teeth_lunch", "teeth_breakfast"),
                'Time "gobedtime"',
                "Parse " + tList2str('OneOfManyFac "meat" {} {}'.format(list2str("cow", "pork", "chicken", "fish", "vegetarian", "vegan"),
                                                                       list2str2("u", "p", "c", "f" ,"v", "V")),
                                    'BinaryPFac "vacuuming" ["vac"]')))

print("d  up   j fr  t bed   parse")
#       8 8:11   r   D 23:14 v

with open(sys.argv[1]) as f:
    print(f.read(), end='')

print("#  up   j fr  t bed   parse")

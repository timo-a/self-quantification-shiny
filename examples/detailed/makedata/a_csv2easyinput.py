import pandas as pd
import sys

def main():
    df = pd.read_csv(sys.argv[1])

    del df['gobedtime_full']

    df['jogging'].fillna(' ', inplace=True)
    for colname in ['french_read', 'french_listen', 'french_speak', 'vacuuming']:
        df[colname].fillna('', inplace=True)

    df['getuptime'] = df['getuptime'].apply(shorten_date)


    df['teeth_breakfast'] = df['teeth_breakfast'].notna().astype(int)
    df['teeth_lunch']     = df['teeth_lunch']    .notna().astype(int)
    df['teeth_dinner']    = df['teeth_dinner']   .notna().astype(int)
    df['teeth_floss']     = df['teeth_floss']    .notna().astype(int)

    #print("date ")

    for _, r in df.iterrows():

        french = ""
        if r['french_read']:
            french += 'r'
        if r['french_listen']:
            french += 'l'
        if r['french_speak']:
            french += 's'
        french = french.ljust(3)

        teeth = r[['teeth_floss', 'teeth_dinner', 'teeth_lunch', 'teeth_breakfast']] @ [8, 4, 2, 1]
        teeth = hex(teeth)[2:].upper()

        parse = vegetarian_dict[r['meat']] + (' vac' if r['vacuuming'] else '')

        newrow = ' '.join((r['date'], r['getuptime'], r['jogging'], french, teeth, r['gobedtime'], parse))
        print(newrow)

def shorten_date(x):
    #since humans get up between 00 and 15:59
    #hour is encoded as one hex char
    return "{:X}".format(int(x[:2]))[-1] + x[2:]

vegetarian_dict = {"vegetarian":"v",
                   "vegan":"V",
                   "fish":"f",
                   "chicken":"c",
                   "pork":"p",
                   "cow":"u"}

if __name__ == '__main__':
    main()

import numpy as np
import itertools as it

import math


def main():
    for m in generate_6_kinds_of_meat_consumption(100):
        print(m)

def generate_6_kinds_of_meat_consumption(n=math.inf):
    
    states = ["vegan", "vegetarian", "fish", "chicken", "pork", "cow"]

    t = np.array([[0.4,   0.2,   0.1,   0.1,   0.1,   0.1  ],
                  [0.2,   0.4,   0.2,   0.2/3, 0.2/3, 0.2/3],
                  [0.2/3, 0.2,   0.4,   0.2,   0.2/3, 0.2/3],
                  [0.2/3, 0.2/3, 0.2,   0.4,   0.2,   0.2/3],
                  [0.2/3, 0.2/3, 0.2/3, 0.2,   0.4,   0.2  ],
                  [0.1,   0.1,   0.1,   0.1,   0.2,   0.4  ]
                 ])

    g = generate_eating(t, states)

    if n == math.inf:
        return g
    else:
        return it.islice(g, n)

def generate_eating(t, states):
    s = np.random.choice(states)
    #import pdb; pdb.set_trace()
    yield(s)
    while True:
        s = np.random.choice(states, p=t[states.index(s)])
        yield(s)

if __name__ == '__main__':
    main()

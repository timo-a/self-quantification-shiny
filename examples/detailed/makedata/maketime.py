import pandas as pd
import datetime as dt

import numpy as np
np.random.seed(0)

import math
import random
import itertools as it


import markov


from_date = "2015-09-08"
to_date   = "2019-11-01" #nach 2017-11-20 NaT


parsedate = lambda x : dt.datetime.strptime(x, '%Y-%m-%d')

day_range = (parsedate(to_date) - parsedate(from_date)).days


dates = pd.Series([parsedate(from_date) + dt.timedelta(days=x) for x in range(day_range)])


min_deviation  = np.round(40 * np.random.randn(1, day_range))#randn generates N(mu=0, var=1); a*N(0,1) +b = N(b, a^2); so 40*N(0,1)=N(0,40^2)
timediff_getup = pd.Series([dt.timedelta(hours=7, minutes=x) for x in min_deviation.flatten()])

min_deviation  = np.round(60 * np.random.randn(1, day_range))
timediff_gobed = pd.Series([dt.timedelta(hours=23, minutes=x) for x in min_deviation.flatten()])
# TODO: back to 22:00?
getuptimes = dates + timediff_getup
gobedtimes = dates + timediff_gobed

df = pd.DataFrame({'date':dates,
                   'getuptime':getuptimes.map(lambda x : x.strftime("%H:%M")),
                   'gobedtime':gobedtimes.map(lambda x : x.strftime("%H:%M")),
                   'gobedtime_full':gobedtimes.map(lambda x : x.strftime("%Y-%m-%d %H:%M"))})

df['meat'] = pd.Series(markov.generate_6_kinds_of_meat_consumption(day_range)) 


def generate_vacuumdays():
    def generate_distances():
        yield(random.randint(1,14))
        while True:
            yield(round(random.normalvariate(mu=13, sigma=3)))

    for d in generate_distances():
        for n in range(d):
            yield('')
        yield('O')
        


df['vacuuming'] = pd.Series(it.islice(generate_vacuumdays(), day_range))

df['jogging']=np.random.choice(["","O"], size=day_range, p=[0.6,0.4])

df['french_read']=np.random.choice(["","O"], size=day_range, p=[0.6,0.4])
df['french_listen']=np.random.choice(["","O"], size=day_range, p=[0.6,0.4])
df['french_speak']=np.random.choice(["","O"], size=day_range, p=[0.6,0.4])

df['teeth_breakfast']=np.random.choice(["","O"], size=day_range, p=[0.6,0.4])
df['teeth_lunch']    =np.random.choice(["","O"], size=day_range, p=[0.8,0.2])
df['teeth_dinner']   =np.random.choice(["","O"], size=day_range, p=[0.4,0.6])
df['teeth_floss']    =np.random.choice(["","O"], size=day_range, p=[0.95,0.05])

df.to_csv('test.csv',sep=',')
